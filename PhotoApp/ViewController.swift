//
//  ViewController.swift
//  PhotoApp
//
//  Created by Debasish Kaushik on 4/22/16.
//  Copyright © 2016 gsu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate{
    
    var oldImage: UIImage?
    
    var newImage: UIImage?

    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var secondaryMenu: UIView!
    
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var RGBButton: UIButton!
    
    @IBOutlet var buttomMenu: UIView!
    
    @IBOutlet var compareButton: UIButton!
    
    @IBOutlet var newMenu: UIView!
    
    @IBOutlet var labelView: UILabel!
    
    var orginal = true
    
    func showOrginalText() {
        if(orginal) {
            labelView.hidden = false
        }
        else {
            labelView.hidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        secondaryMenu.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        secondaryMenu.translatesAutoresizingMaskIntoConstraints = false
        
        newMenu.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        newMenu.translatesAutoresizingMaskIntoConstraints = false
        
        zoomOnDoubleTap.numberOfTapsRequired = 2
        
        compareButton.userInteractionEnabled  = false
        compareButton.alpha = 0.5
        
        filterButton.userInteractionEnabled = false
        filterButton.alpha = 0.5
        
        showOrginalText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onFilter(sender: UIButton) {
        
        if (sender.selected) {
            
            hideNewMenu()
            sender.selected = false
            
        } else {
            
            showNewMenu()
            sender.selected = true
        
        }
        
    }
    
    
    
    @IBAction func onRGBFilter(sender: UIButton) {
        if (sender.selected) {
            
            hideSecondaryMenu()
            sender.selected = false
            
        } else {
            
            showSecondaryMenu()
            sender.selected = true
        
        }
    }
    
    
    func showNewMenu() {
        
        view.addSubview(newMenu)
        
        let bottomConstraint = newMenu.bottomAnchor.constraintEqualToAnchor(buttomMenu.topAnchor)
        
        let leftConstraint = newMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        
        let rightConstraint = newMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        
        let heightConstraint = newMenu.heightAnchor.constraintEqualToConstant(44)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        
        view.layoutIfNeeded()
        
        self.newMenu.alpha = 0
        
        UIView.animateWithDuration(0.3) {
            self.newMenu.alpha = 1.0
            
            if self.secondaryMenu.hidden == false {
                self.hideSecondaryMenu()
            }
        }
    }
    
    
    func hideNewMenu() {
        UIView.animateWithDuration(0.3, animations: {
            self.newMenu.alpha = 0
        }) { completed in
            if completed == true {
                self.newMenu.removeFromSuperview()
            }
        }
    }
    
    
    func showSecondaryMenu() {
        
        view.addSubview(secondaryMenu)
        
        let bottomConstraint = secondaryMenu.bottomAnchor.constraintEqualToAnchor(buttomMenu.topAnchor)
        
        let leftConstraint = secondaryMenu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        
        let rightConstraint = secondaryMenu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        
        let heightConstraint = secondaryMenu.heightAnchor.constraintEqualToConstant(44)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, leftConstraint, rightConstraint, heightConstraint])
        
        view.layoutIfNeeded()
        
        self.secondaryMenu.alpha = 0
        
        UIView.animateWithDuration(0.3) {
            self.secondaryMenu.alpha = 1.0
            
            if self.newMenu.hidden == false {
                self.hideNewMenu()
            }
            
        }
        
    }
    
    func hideSecondaryMenu() {
        UIView.animateWithDuration(0.3, animations: {
            self.secondaryMenu.alpha = 0
        }) { completed in
            if completed == true {
                self.secondaryMenu.removeFromSuperview()
            }
        }
    }
    

    @IBAction func onNewPhoto(sender: AnyObject) {
        
        let actionSheet = UIAlertController(title: "New Photo", message: nil, preferredStyle: .ActionSheet)
        
        actionSheet.addAction(UIAlertAction(title:"Camera", style: .Default, handler: {
            action in
            self.showCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Album", style: .Default, handler: {
            action in
            self.showAlbum()
        }))
        
        actionSheet.addAction(UIAlertAction(title:"Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(actionSheet, animated: true, completion: nil)
        
        orginal = true
        showOrginalText()
        
        oldImage = imageView.image
    }
    
    
    func showCamera() {
    
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .Camera
        self.presentViewController(cameraPicker, animated: true, completion: nil)

    }
    
    
    func showAlbum() {
        
        let albumPicker = UIImagePickerController()
        albumPicker.delegate = self
        albumPicker.sourceType = .PhotoLibrary
        self.presentViewController(albumPicker, animated: true, completion: nil)

    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            imageView.image = image
            newImage = image
            orginal = true
            showOrginalText()
        }
        
    }
    
    @IBAction func onShare(sender: AnyObject) {
        
        let activityController = UIActivityViewController(activityItems: [imageView.image!], applicationActivities: nil)
        presentViewController(activityController, animated: true, completion: nil)
        
    }
    
    @IBOutlet var brightnessSlider: UISlider!
    
    var preSliderVal: Float = 125
    
    
    @IBAction func onStartSliding(sender: AnyObject) {
        preSliderVal = sender.value
    }
    
    @IBAction func onDragBrightness(sender: UISlider){
        
        let value = sender.value
        let image = imageView.image!
        oldImage = image
    
        var rgbaImage = RGBAImage(image: image)!
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let Delta = (value - preSliderVal)/255
                
                pixel.red = UInt8(min(255,max(1,round(Float(pixel.red) * (1 + Delta)))))
                pixel.green = UInt8(min(255,max(1,round(Float(pixel.green) * (1 + Delta)))))
                pixel.blue = UInt8(min(255,max(1,round(Float(pixel.blue) * (1 + Delta)))))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        newImage = rgbaImage.toUIImage()
        imageView.image = newImage
        
        preSliderVal = value
        
        UIButton.animateWithDuration(0.4) {
            self.compareButton.alpha = 1.0
            self.compareButton.userInteractionEnabled = true
            
            self.filterButton.alpha = 1.0
            self.filterButton.userInteractionEnabled = true
        }
        
        orginal = false
        showOrginalText()
    }
    
    
    @IBAction func onCompare(sender: AnyObject) {
        
      if orginal {
            orginal = false
            imageView.image = newImage
        } else {
            orginal = true
            imageView.image = oldImage
        }
        
        showOrginalText()
    }
    
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    @IBOutlet var zoomOnDoubleTap: UITapGestureRecognizer!
    
    @IBOutlet var imageScrollView: UIScrollView!
    
    @IBAction func onTapToZoom(sender: AnyObject) {
        
        UIView.animateWithDuration(0.5) { () -> Void in
            //self.imageScrollView.zoomScale = 1.5 * self.imageScrollView.zoomScale }
       
      
            if self.orginal {
                self.orginal = false
                self.imageView.image = self.newImage
            } else {
                self.orginal = true
                self.imageView.image = self.oldImage
            }
        }

        showOrginalText()
    }
    
    
    
    
    @IBAction func onBlueFilter(sender: AnyObject) {

        let image = imageView.image!
        oldImage = image
        
        var rgbaImage = RGBAImage(image: image)!
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let Delta = Int(pixel.blue) - 125
                
                var modifier = 1 + 4 * (Double(y) / Double(rgbaImage.height))
                if(Int(pixel.blue) < Delta) {
                    modifier = 1
                }
                
                
                pixel.blue = UInt8(max(0,min(255,round(125.0 + modifier * Double(Delta)))))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        newImage = rgbaImage.toUIImage()
        
        UIButton.animateWithDuration(0.4) {
            self.compareButton.alpha = 1.0
            self.compareButton.userInteractionEnabled = true
            
            self.filterButton.alpha = 1.0
            self.filterButton.userInteractionEnabled = true
        }
        
        //UIImage.animatedImageWithImages([rgbaImage.toUIImage()!], duration: 0.4)
        
        imageView.image = newImage
        
        orginal = false
        showOrginalText()
    }
    
    
    @IBAction func onGreenFilter(sender: AnyObject) {
        
        let image = imageView.image!
        oldImage = image
        
        var rgbaImage = RGBAImage(image: image)!
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let Delta = Int(pixel.green) - 125
                
                var modifier = 1 + 4 * (Double(y) / Double(rgbaImage.height))
                if(Int(pixel.green) < Delta) {
                    modifier = 1
                }
                
                
                pixel.green = UInt8(max(0,min(255,round(125.0 + modifier * Double(Delta)))))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        UIButton.animateWithDuration(0.4) {
            self.compareButton.alpha = 1.0
            self.compareButton.userInteractionEnabled = true
            
            self.filterButton.alpha = 1.0
            self.filterButton.userInteractionEnabled = true
        }
        
        
        //UIImage.animatedImageWithImages([rgbaImage.toUIImage()!], duration: 0.4)
        newImage = rgbaImage.toUIImage()
        imageView.image = newImage
        
        orginal = false
        showOrginalText()
    }
    
    
    @IBAction func onRedInside(sender: AnyObject) {
        
        let image = imageView.image!
        oldImage = image
        
        var rgbaImage = RGBAImage(image: image)!
        
        for y in 0..<rgbaImage.height {
            for x in 0..<rgbaImage.width {
                let index = y * rgbaImage.width + x
                var pixel = rgbaImage.pixels[index]
                
                let Delta = Int(pixel.red) - 125
                
                var modifier = 1 + 4 * (Double(y) / Double(rgbaImage.height))
                if(Int(pixel.red) < Delta) {
                    modifier = 1
                }
                
                
                pixel.red = UInt8(max(0,min(255,round(125.0 + modifier * Double(Delta)))))
                
                rgbaImage.pixels[index] = pixel
            }
        }
        
        UIButton.animateWithDuration(0.4) {
            self.compareButton.alpha = 1.0
            self.compareButton.userInteractionEnabled = true
            
            self.filterButton.alpha = 1.0
            self.filterButton.userInteractionEnabled = true
        }
        
        //UIImage.animatedImageWithImages([rgbaImage.toUIImage()!], duration: 0.4)
        newImage = rgbaImage.toUIImage()
        imageView.image = newImage
        
        orginal = false
        showOrginalText()
        
    }
    
}








